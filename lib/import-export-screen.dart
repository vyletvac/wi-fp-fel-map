import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ImportExportScreen extends StatefulWidget {
  const ImportExportScreen({super.key});

  @override
  State<ImportExportScreen> createState() => _ImportExportScreenState();
}

class _ImportExportScreenState extends State<ImportExportScreen> {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  Future<File?> _file = Future(() => null);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Text("Editing file:"),
        FutureBuilder<File?>(
            future: _file,
            builder: (context, snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                case ConnectionState.waiting:
                  return const LinearProgressIndicator();
                case ConnectionState.active:
                case ConnectionState.done:
                  if (snapshot.hasError) {
                    return Text('Error: ${snapshot.error}');
                  } else {
                    // print("file is ${snapshot.data!.uri.path}");
                    return Text(snapshot.data == null
                        ? "No file found"
                        : snapshot.data?.uri.toString() ??
                            "No file found");
                  }
              }
            }),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                FilePicker.platform
                    .saveFile(

                        fileName:
                            "wifi-fp_${DateTime.now().toLocal().toString().substring(0, 10)}.wbfp")
                    .then(
                  (value) {
                    _prefs.then(
                      (prefs) {
                        setState(() {
                          _file = Future(() => value == null ? null:File(value));
                          prefs.setString("file", value ?? "");
                        });
                      },
                    );
                  },
                );
              },
              child: const Text("New file"),
            ),
            ElevatedButton(
                onPressed: () {
                  FilePicker.platform
                      .pickFiles(type: FileType.any)
                      .then((result) {
                    if (result == null) {
                      _prefs.then(
                        (prefs) {
                          setState(() {
                            prefs.setString("file", "");
                          });
                        },
                      );
                    } else {
                      String? path = result.files.last.path;
                      _prefs.then(
                        (prefs) {
                          setState(() {
                            _file = Future(() => path == null ? null:File(path));

                            prefs.setString("file", path ?? "");
                          });
                        },
                      );
                    }
                  });
                },
                child: const Text("Open file")),
          ],
        ),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    _file = _prefs.then((SharedPreferences prefs) {
      String? path = prefs.getString("file");
      if (path == null || path == "") {
        return null;
      } else {
        return File(path);
      }
    });
  }
}
