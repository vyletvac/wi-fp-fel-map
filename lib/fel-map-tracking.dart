import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wi_fp_fel_map/map-point-data.dart';
import 'package:wi_fp_fel_map/wbfp-helper-functions.dart';
import 'package:wifi_scan/wifi_scan.dart';

import 'map-point.dart';

class FelMapTracking extends StatefulWidget {
  const FelMapTracking({super.key});

  @override
  State<FelMapTracking> createState() => _FelMapTrackingState();
}

class _FelMapTrackingState extends State<FelMapTracking> {
  SvgPicture mapSvg = SvgPicture.asset(
    "assets/felMap.svg",
    width: 1000,
  );
  late Timer scanningTimer;
  List<MapPointData> scannedPoints = [];

  @override
  void initState() {
    WiFiScan.instance.startScan();
    scanningTimer = Timer.periodic(const Duration(seconds: 10), (timer) {
      WiFiScan.instance.startScan();
    });
    loadPointsFromFile().then(
      (value) {
        scannedPoints = value;
      },
    );
    super.initState();
  }

  @override
  void dispose() {
    scanningTimer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<WiFiAccessPoint>>(
        stream: WiFiScan.instance.onScannedResultsAvailable,
        builder: (context, snapshot) {
          WiFiScan.instance.startScan();
          List<WiFiAccessPointData> scannedAPs = [];
          if (snapshot.data == null) {
            scannedAPs = [];
          } else {
            scannedAPs =
                snapshot.data!.map((e) => WiFiAccessPointData(e)).toList();
          }

          MapPointData? nearestPoint = findNearestPoint(scannedAPs);
          return Stack(children: <Widget>[
            InteractiveViewer(
              maxScale: 10,
              child: Stack(
                  children: <Widget>[mapSvg] +
                      ((nearestPoint == null)
                          ? List.empty()
                          : [MapPoint(nearestPoint)])),
            ),
          ]);
        });
  }

  MapPointData? findNearestPoint(List<WiFiAccessPointData> currentAps) {
    Map<double, MapPointData> distances = {
      for (MapPointData element in scannedPoints)
        wifiFpDistance(element.accessPoints, currentAps): element
    };
    List<double> keys = distances.keys.toList();
    keys.sort();
    if (keys.isEmpty) {
      return null;
    }
    return distances[keys[0]];
  }

  double wifiFpDistance(
      List<WiFiAccessPointData> a, List<WiFiAccessPointData> b) {
    List<String> commonApsBssids = a
        .where(
          (ael) => b.any((bel) => bel.bssid == ael.bssid),
        )
        .map((e) => e.bssid)
        .toList();
    double distanceSquared = 0;
    //add distance of common aps
    for (String commonBssid in commonApsBssids) {
      WiFiAccessPointData aCommonAP =
          a.firstWhere((element) => element.bssid == commonBssid);
      WiFiAccessPointData bCommonAP =
          b.firstWhere((element) => element.bssid == commonBssid);

      double levelDiff = (aCommonAP.level - bCommonAP.level).toDouble();
      distanceSquared += levelDiff * levelDiff;
    }

    //add distance of aps in only one point
    for (String commonBssid in commonApsBssids) {
      a.removeWhere((element) => element.bssid == commonBssid);
    }
    for (WiFiAccessPointData ap in a) {
      distanceSquared += ap.level * ap.level;
    }
    for (String commonBssid in commonApsBssids) {
      b.removeWhere((element) => element.bssid == commonBssid);
    }
    for (WiFiAccessPointData ap in b) {
      distanceSquared += ap.level * ap.level;
    }

    return sqrt(distanceSquared);
  }
}
