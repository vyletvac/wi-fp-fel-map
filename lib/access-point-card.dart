import 'package:flutter/material.dart';

import 'map-point-data.dart';

class AccessPointCard extends StatelessWidget {
  const AccessPointCard(this.ap, {super.key});

  final WiFiAccessPointData ap;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Row(
        children: [
          const SizedBox(width: 3),
          Column(

            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(ap.ssid.length<14?ap.ssid:"${ap.ssid.substring(0,14)}...",style: const TextStyle(fontSize: 9)),
              Text(ap.bssid.toString(),style: const TextStyle(fontSize: 9)),
            ],
          ),
          const VerticalDivider(),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("f=${ap.frequency} MHz",style: const TextStyle(fontSize: 9)),
              Text("RSSI=${ap.level} dBm",style: const TextStyle(fontSize: 9))
            ],
          ),

          Flexible(
            child: LinearProgressIndicator(value: (100 + ap.level) / 100.0+0.5),
          ),
          const SizedBox(width: 3,),

        ],
      ),
    );
  }
}
