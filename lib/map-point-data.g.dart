// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map-point-data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MapPointData _$MapPointDataFromJson(Map<String, dynamic> json) => MapPointData(
      (json['xPos'] as num).toDouble(),
      (json['yPos'] as num).toDouble(),
      accessPoints: (json['accessPoints'] as List<dynamic>)
          .map((e) => WiFiAccessPointData.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MapPointDataToJson(MapPointData instance) =>
    <String, dynamic>{
      'xPos': instance.xPos,
      'yPos': instance.yPos,
      'accessPoints': instance.accessPoints,
    };

WiFiAccessPointData _$WiFiAccessPointDataFromJson(Map<String, dynamic> json) =>
    WiFiAccessPointData.fromFields(
      ssid: json['ssid'] as String,
      bssid: json['bssid'] as String,
      capabilities: json['capabilities'] as String,
      standard: $enumDecode(_$WiFiStandardsEnumMap, json['standard']),
      level: (json['level'] as num).toInt(),
      channelWidth:
          $enumDecodeNullable(_$WiFiChannelWidthEnumMap, json['channelWidth']),
      frequency: (json['frequency'] as num).toInt(),
      centerFrequency0: (json['centerFrequency0'] as num?)?.toInt(),
      centerFrequency1: (json['centerFrequency1'] as num?)?.toInt(),
      timestamp: (json['timestamp'] as num?)?.toInt(),
      isPasspoint: json['isPasspoint'] as bool?,
      operatorFriendlyName: json['operatorFriendlyName'] as String?,
      venueName: json['venueName'] as String?,
      is80211mcResponder: json['is80211mcResponder'] as bool?,
    );

Map<String, dynamic> _$WiFiAccessPointDataToJson(
        WiFiAccessPointData instance) =>
    <String, dynamic>{
      'ssid': instance.ssid,
      'bssid': instance.bssid,
      'capabilities': instance.capabilities,
      'standard': _$WiFiStandardsEnumMap[instance.standard]!,
      'level': instance.level,
      'channelWidth': _$WiFiChannelWidthEnumMap[instance.channelWidth],
      'frequency': instance.frequency,
      'centerFrequency0': instance.centerFrequency0,
      'centerFrequency1': instance.centerFrequency1,
      'timestamp': instance.timestamp,
      'isPasspoint': instance.isPasspoint,
      'operatorFriendlyName': instance.operatorFriendlyName,
      'venueName': instance.venueName,
      'is80211mcResponder': instance.is80211mcResponder,
    };

const _$WiFiStandardsEnumMap = {
  WiFiStandards.unkown: 'unkown',
  WiFiStandards.legacy: 'legacy',
  WiFiStandards.n: 'n',
  WiFiStandards.ac: 'ac',
  WiFiStandards.ax: 'ax',
  WiFiStandards.ad: 'ad',
};

const _$WiFiChannelWidthEnumMap = {
  WiFiChannelWidth.unkown: 'unkown',
  WiFiChannelWidth.mhz20: 'mhz20',
  WiFiChannelWidth.mhz40: 'mhz40',
  WiFiChannelWidth.mhz80: 'mhz80',
  WiFiChannelWidth.mhz160: 'mhz160',
  WiFiChannelWidth.mhz80Plus80: 'mhz80Plus80',
};
