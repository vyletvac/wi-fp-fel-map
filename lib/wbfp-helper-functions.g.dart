// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wbfp-helper-functions.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WbfpFile _$WbfpFileFromJson(Map<String, dynamic> json) => WbfpFile(
      json['buildingName'] as String,
      (json['points'] as List<dynamic>)
          .map((e) => MapPointData.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$WbfpFileToJson(WbfpFile instance) => <String, dynamic>{
      'buildingName': instance.buildingName,
      'points': instance.points,
    };
