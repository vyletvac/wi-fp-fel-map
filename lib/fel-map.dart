import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wi_fp_fel_map/deletepoint-menu.dart';
import 'package:wi_fp_fel_map/map-point-data.dart';
import 'package:wi_fp_fel_map/wbfp-helper-functions.dart';
import 'package:wifi_scan/wifi_scan.dart';

import 'new-point-menu.dart';
import 'map-point.dart';

class FelMap extends StatefulWidget {
  const FelMap({super.key});

  @override
  State<FelMap> createState() => _FelMapState();
}

class _FelMapState extends State<FelMap> {
  bool creatingPoint = false;
  SvgPicture mapSvg = SvgPicture.asset(
    "assets/felMap.svg",
    width: 1000,
  );
  List<MapPoint> points = [];
  List<DeletePointMenu> deletePointMenus = [];

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<MapPointData>>(
        future: loadPointsFromFile(),
        initialData: const [],
        builder: (context, snapshot) {
          // if(snapshot.connectionState != ConnectionState.done)
          //   {
          //     // return Center(child: Text(snapshot.toString()),);
          //   }
          try{
            points = snapshot.data!
                .map((e) => MapPoint(
                      e,
                      onTap: () {
                        onPermanentPointTap(e);
                      },
                    ))
                .toList();
          }
          catch (e){
            points = [];
          }
          return Stack(
              children: <Widget>[
                    InteractiveViewer(
                      maxScale: 10,
                      child: Stack(
                        children: <Widget>[
                              GestureDetector(
                                  onLongPressStart: (details) {
                                    setState(() {
                                      // points.add(_Point(details.localPosition.dx, details.localPosition.dy));
                                      if (creatingPoint) {
                                        return;
                                      } else {
                                        creatingPoint = true;
                                        showTemporaryPoint(
                                            details.localPosition.dx,
                                            details.localPosition.dy);
                                        createNewPointMenu();
                                      }
                                    });
                                  },
                                  child: mapSvg),
                            ] +
                            points +
                            temporaryPoints +
                            deletePointMenus,
                      ),
                    ),
                  ] +
                  newPointMenus);
        });
  }

  List<MapPoint> temporaryPoints = List.empty(growable: true);

  void showTemporaryPoint(double xPos, double yPos) {
    temporaryPoints.add(MapPoint(
      MapPointData(xPos, yPos, accessPoints: []),
      temporary: true,
    ));
  }

  List<Widget> newPointMenus = List.empty(growable: true);

  void createNewPointMenu() {
    removeDeletePointMenu();
    newPointMenus.add(Align(
      alignment: Alignment.bottomCenter,
      child: NewPointMenu(
        onAcceptPressed: pointAccepted,
        onCancelPressed: pointRefused,
      ),
    ));
  }

  void removeNewPointMenu() {
    newPointMenus.removeLast();
    creatingPoint = false;
  }

  void pointAccepted(List<WiFiAccessPoint> aps) {
    setState(() {
      MapPoint temporaryPoint = temporaryPoints.last;
      temporaryPoint.pointData.accessPoints =
          aps.map((e) => WiFiAccessPointData(e)).toList();

      MapPoint permanentPoint = MapPoint(
        temporaryPoint.pointData,
        onTap: () {
          onPermanentPointTap(temporaryPoint.pointData);
        },
      );
      addPointToFile(permanentPoint.pointData);
      removeNewPointMenu();
      temporaryPoints.removeLast();
    });
  }

  void pointRefused() {
    setState(() {
      temporaryPoints.removeLast();
      removeNewPointMenu();
    });
  }

  void createDeletePointMenu(MapPointData point) {
    setState(() {
      deletePointMenus.add(DeletePointMenu(
        point: point,
        onDeletePressed: () {
          deletePointFromFile(point);
          removeDeletePointMenu();
        },
        onCancelPressed: () {
          removeDeletePointMenu();
        },
      ));
    });
  }

  void removeDeletePointMenu() {
    setState(() {
      deletePointMenus = [];
    });
  }

  void onPermanentPointTap(MapPointData point) {
    if (deletePointMenus.isEmpty) {
      createDeletePointMenu(point);
    } else {
      removeDeletePointMenu();
      createDeletePointMenu(point);
    }
  }
}
