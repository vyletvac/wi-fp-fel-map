import 'package:flutter/material.dart';
import 'package:wi_fp_fel_map/access-point-card.dart';
import 'package:wi_fp_fel_map/map-point-data.dart';


class DeletePointMenu extends StatelessWidget {
  const DeletePointMenu(
      {super.key,
      this.onDeletePressed,
      required this.point,
      this.onCancelPressed});

  final void Function()? onDeletePressed;
  final void Function()? onCancelPressed;
  final MapPointData point;

  @override
  Widget build(BuildContext context) {
    return Positioned(
        top: point.yPos-4,
        left: point.xPos > 330 ? point.xPos-(0.4*250)+4 : point.xPos-4,
        child: Transform.scale(
          alignment: Alignment.topLeft,
          scale: 0.4,
          child: SizedBox(
            width: 250,
            height: 400,
            // child: Placeholder(),
            child: Card(
                color: Colors.white70,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    ElevatedButton(
                        onPressed: onCancelPressed, child: const Text("Cancel")),
                    ElevatedButton(
                        onPressed: onDeletePressed, child: const Text("Delete")),
                    Expanded(child: ListView(children: point.accessPoints.map((e) => AccessPointCard(e)).toList(),))
                  ],
                )),
          ),
        ));
  }
}
