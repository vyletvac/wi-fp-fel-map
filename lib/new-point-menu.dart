import 'package:flutter/material.dart';
import 'package:timer_count_down/timer_count_down.dart';
import 'package:wi_fp_fel_map/access-point-card.dart';
import 'package:wi_fp_fel_map/map-point-data.dart';
import 'package:wifi_scan/wifi_scan.dart';
const int DELAY_SECONDS = 10;
void _startScan() async {
  CanStartScan canStartScan =
      await WiFiScan.instance.canStartScan(askPermissions: true);

  if (canStartScan == CanStartScan.yes) {
    WiFiScan.instance.startScan();
  } else {
  }
}

class NewPointMenu extends StatefulWidget {
  final void Function(List<WiFiAccessPoint>)? onAcceptPressed;
  final void Function()? onCancelPressed;

  List<WiFiAccessPoint> accessPoints = [];

  NewPointMenu({super.key, this.onAcceptPressed, this.onCancelPressed}) {
    _startScan();
  }

  @override
  State<NewPointMenu> createState() => _NewPointMenuState();
}

class _NewPointMenuState extends State<NewPointMenu> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: FractionallySizedBox(
        widthFactor: 1,
        heightFactor: 0.3,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.white70,
          ),
          child: Column(
            children: [
              const Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Meassure new point",
                    style: TextStyle(color: Colors.black, fontSize: 28),
                  )),
              Flexible(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _DelayedButton(
                          onPressed:
                              widget.onAcceptPressed == null ? null : () {
                              widget.onAcceptPressed!(widget.accessPoints);
                              },
                        ),
                        Container(
                          margin: const EdgeInsets.all(5),
                          child: ElevatedButton(
                            onPressed: widget.onCancelPressed,
                            child: const Text("Cancel"),
                          ),
                        ),
                      ],
                    ),
                    Flexible(
                      child: StreamBuilder<List<WiFiAccessPoint>>(
                          stream: WiFiScan.instance.onScannedResultsAvailable,
                          builder: (context, snapshot) {
                            widget.accessPoints = snapshot.data ?? List.empty();
                            // return ListView(children: [Placeholder(),Placeholder()],);
                            return Container(
                                margin: const EdgeInsets.all(5),
                                child: ListView(
                                  children: widget.accessPoints
                                      .map((e) =>
                                          Flexible(child: AccessPointCard(WiFiAccessPointData(e))))
                                      .toList(),
                                ));
                          }),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _DelayedButton extends StatelessWidget {
  const _DelayedButton({super.key, this.onPressed});

  final Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return Countdown(
        seconds: DELAY_SECONDS,
        interval: const Duration(milliseconds: 20),
        build: (BuildContext context, double timeLeft) {
          return Row(
            children: [
              Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: ElevatedButton(
                      onPressed: (timeLeft == 0) ? onPressed : null,
                      child: const Text("Accept"))),
              CircularProgressIndicator(
                value: timeLeft / DELAY_SECONDS,
              )
            ],
          );
        });
  }
}
