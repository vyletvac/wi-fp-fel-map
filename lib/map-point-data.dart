import 'package:wifi_scan/wifi_scan.dart';
import 'package:json_annotation/json_annotation.dart';

part 'map-point-data.g.dart';

@JsonSerializable()
class MapPointData {
  factory MapPointData.fromJson(Map<String, dynamic> json) => _$MapPointDataFromJson(json);
  Map<String,dynamic> toJson() => _$MapPointDataToJson(this);

  double xPos;
  double yPos;
  List<WiFiAccessPointData> accessPoints;

  MapPointData(this.xPos, this.yPos, {required this.accessPoints});
}

@JsonSerializable(constructor: "fromFields" )
class WiFiAccessPointData {

  factory WiFiAccessPointData.fromJson(Map<String, dynamic> json) => _$WiFiAccessPointDataFromJson(json);
  Map<String,dynamic> toJson() => _$WiFiAccessPointDataToJson(this);

  String ssid;
  String bssid;
  String capabilities;
  WiFiStandards standard;
  int level;
  WiFiChannelWidth? channelWidth;
  int frequency;
  int? centerFrequency0;
  int? centerFrequency1;
  int? timestamp;
  bool? isPasspoint;
  String? operatorFriendlyName;
  String? venueName;
  bool? is80211mcResponder;

  WiFiAccessPointData.fromFields(
      {required this.ssid,
      required this.bssid,
      required this.capabilities,
      required this.standard,
      required this.level,
       this.channelWidth,
      required this.frequency,
       this.centerFrequency0,
       this.centerFrequency1,
       this.timestamp,
       this.isPasspoint,
       this.operatorFriendlyName,
       this.venueName,
       this.is80211mcResponder});

  WiFiAccessPointData(WiFiAccessPoint point)
      : ssid = point.ssid,
        bssid = point.bssid,
        capabilities = point.capabilities,
        standard = point.standard,
        level = point.level,
        channelWidth = point.channelWidth,
        frequency = point.frequency,
        centerFrequency0 = point.centerFrequency0,
        centerFrequency1 = point.centerFrequency1,
        timestamp = point.timestamp,
        isPasspoint = point.isPasspoint,
        operatorFriendlyName = point.operatorFriendlyName,
        venueName = point.venueName,
        is80211mcResponder = point.is80211mcResponder;
}
