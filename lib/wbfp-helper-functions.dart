import 'dart:io';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:json_annotation/json_annotation.dart';

import 'map-point-data.dart';

part "wbfp-helper-functions.g.dart";

Future<List<MapPointData>> loadPointsFromFile() async {
  WbfpFile file = await _getFile();
  return file.points;
}

void addPointToFile(MapPointData pointData) async {
  WbfpFile file = await _getFile();
  int pointIndex = file.points.indexWhere((element) =>
      element.xPos == pointData.xPos && element.yPos == pointData.yPos);
  if (pointIndex == -1) {
    file.points.add(pointData);
  } else {
    file.points[pointIndex] = pointData;
  }
  _saveFile(file);
}

void deletePointFromFile(MapPointData pointData) async {
  WbfpFile file = await _getFile();
  int pointIndex = file.points.indexWhere((element) =>
      element.xPos == pointData.xPos && element.yPos == pointData.yPos);
  if (pointIndex == -1) {
    throw (Exception("point does not exist"));
  } else {
    file.points.removeAt(pointIndex);
  }
  _saveFile(file);
}

Future<WbfpFile> _getFile() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs.getString("file") == null || prefs.getString("file") == "") {
    throw const FileSystemException("File does not exist");
  }
  File file = File(prefs.getString("file")!);

  Map<String, dynamic> json;
  try {
    json = jsonDecode(await file.readAsString());
  } catch (exception) {
    WbfpFile dummy = WbfpFile("FEL", []);
    _saveFile(dummy);
    return dummy;
  }
  return WbfpFile.fromJson(json);
}

void _saveFile(WbfpFile wbfpFile) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs.getString("file") == null || prefs.getString("file") == "") {
    throw Exception("File does not exist");
  }
  File file = File(prefs.getString("file")!);
  file.writeAsString(jsonEncode(wbfpFile));
}

@JsonSerializable()
class WbfpFile {
  String buildingName;
  List<MapPointData> points;

  WbfpFile(this.buildingName, this.points);

  factory WbfpFile.fromJson(Map<String, dynamic> json) =>
      _$WbfpFileFromJson(json);

  Map<String, dynamic> toJson() => _$WbfpFileToJson(this);
}
