import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:wi_fp_fel_map/map-point-data.dart';


class MapPoint extends StatelessWidget {
   const MapPoint( this.pointData, {super.key, this.temporary = false,this.onTap,});

  final MapPointData pointData;
  final bool temporary;
  final Function()? onTap;

  get xPos => pointData.xPos;
  get yPos => pointData.yPos;


  @override
  String toString({ DiagnosticLevel minLevel = DiagnosticLevel.info }) {
    return 'MapPoint{xPos: ${pointData.xPos}, yPos: ${pointData.yPos}, temporary: $temporary, onTapDown: $onTap, accessPoints: ${pointData.accessPoints}}';
  }

   @override
  Widget build(BuildContext context) {
    if (temporary) {
      return Positioned(
          left: pointData.xPos - 5,
          top: pointData.yPos - 5,
          child: Container(
            width: 10,
            height: 10,
            decoration: BoxDecoration(
              color: Colors.red,
              borderRadius: BorderRadius.circular(5),
            ),
          ).animate().scale());
    } else {
      return Positioned(
          left: pointData.xPos - 5,
          top: pointData.yPos - 5,
          child: InkResponse(
            highlightShape: BoxShape.circle,

            onTap: onTap,
            child: Container(
              width: 10,
              height: 10,
              decoration: BoxDecoration(
                color: Colors.green,
                borderRadius: BorderRadius.circular(5),
              ),
            ),
          ));
    }
  }
}
